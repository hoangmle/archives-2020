# Commands for updating the image:
# (1) docker build --pull -t registry.gitlab.com/sosy-lab/test-comp/archives-2020/user:latest - < Dockerfile.user
# (2) Create a Gitlab token under https://gitlab.com/profile/personal_access_tokens
#     - Name: "dockerSVCOMParchives" or so
#     - ExpiresAt: e.g., 2020-05-31
#     - Scope: API is sufficient
#     IMPORTANT: Remember new secret token!
# (3) docker login registry.gitlab.com
#     - Username=dbeyer
#     - Password: above generated secret token
# (4) docker push registry.gitlab.com/sosy-lab/test-comp/archives-2020/user:latest

FROM ubuntu:18.04
ENV DEBIAN_FRONTEND=noninteractive
RUN dpkg --add-architecture i386
RUN apt-get update && apt-get install -y \
    openjdk-11-jdk-headless \
    gcc-multilib \
    libgomp1 \
    make \
    clang \
    clang-6.0 \
    llvm-6.0 \
    mono-devel \
    gcc-5-multilib \
    python \
    python-lxml \
    linux-libc-dev:i386 \
    python-sklearn \
    python-pandas \
    python-pycparser \
    python3-setuptools \
    unzip
RUN ln -fs /usr/share/zoneinfo/Europe/Berlin /etc/localtime
RUN dpkg-reconfigure --frontend noninteractive tzdata

# Requirements from SV-COMP'19 and Test-Comp'19:
#
# (Fetch latest version from the Ansible configuration for the competition machines:
#  https://gitlab.com/sosy-lab/admin/sysadmin/ansible/blob/master/roles/benchmarking/tasks/main.yml)
#
#      - openjdk-11-jdk-headless
#      - gcc-multilib # cpp, development headers
#      - libgomp1 # for Z3
#      - make # for fshellw2t
#      - clang # SV-COMP'19 AProVE
#      - clang-6.0 # Test-Comp'19
#      - llvm-6.0 # Test-Comp'19
#      - mono-devel # SV-COMP'19 AProVE, SMACK
#      - gcc-5-multilib # SV-COMP'19 PredatorHP
#      - python # SV-COMP'19 PredatorHP, Symbiotic
#      - python-lxml # SV-COMP'19 Symbiotic
#      - linux-libc-dev:i386 # SV-COMP'19 CBMC
#      - python-sklearn # SV-COMP'19 VeriFuzz
#      - python-pandas # SV-COMP'19 VeriFuzz
#      - python-pycparser # SV-COMP'19 CSeq
#      - python3-setuptools # SecC
#      - unzip # SV-COMP'19 JBMC


